/*
 * Test1.h
 *
 *  Created on: 24 ���. 2015 �.
 *      Author: Diralf
 */

#ifndef DIR1_TEST1_H_
#define DIR1_TEST1_H_

class Test1 {
    int number;
public:
    Test1();
    Test1(int number);
    virtual ~Test1();

    inline int getNumber() {
        return number;
    }
};

#endif /* DIR1_TEST1_H_ */
