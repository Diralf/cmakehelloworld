/*
 * Test2.h
 *
 *  Created on: 24 ���. 2015 �.
 *      Author: Diralf
 */

#ifndef DIR2_TEST2_H_
#define DIR2_TEST2_H_

#include <string>

using namespace std;

class Test2 {
    string text;
public:
    Test2();
    virtual ~Test2();

    inline string getText() {
        return text;
    }
};

#endif /* DIR2_TEST2_H_ */
