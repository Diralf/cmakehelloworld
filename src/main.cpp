//============================================================================
// Name        : makefileexample.cpp
// Author      : Dmitry Kazlou (Diralf)
// Version     :
// Copyright   : EastAreaGames
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <dir1/test1.hpp>
#include <dir2/Test2.h>
using namespace std;

int main() {
    Test1 *test = new Test1(8);
	Test2 *text = new Test2();

	cout << text->getText() << " = " << test->getNumber() << endl; // prints !!!Hello World!!!

	delete test;
    delete text;

    return 0;
}
